// Inicialização do canvas
var canvas = document.getElementById('canvas'); // Pega o elemento de ID "canvas" do html
var ctx = canvas.getContext('2d'); // Inicializa o canvas

// Coloca o canvas no tamanho da tela
ctx.canvas.width = window.innerWidth; // Largura
ctx.canvas.height = window.innerHeight; // Altura

/* Inicializa o raio inicial, posição inicial, cor e velocidade */
var radius = 35; // Raio
x = Math.random() * (window.innerWidth - radius * 2) + radius; // Tamanho da largura
y = Math.random() * (window.innerHeight - radius * 2) + radius; // Tamanho da altura

var dx = dy = velocity = 10; // Velocidade
var color = '#' + Math.floor(Math.random() * 16777215).toString(16); // Cor aleatória

// Desenha o círculo na tela
function draw() {
    ctx.beginPath(); // Começa a desenhar
    ctx.arc(x, y, radius, 0, 2 * Math.PI); // Desenha o círculo
    ctx.stroke(); // Traceja o círculo (circunferência)
    ctx.fillStyle = color; // Cor do preenchimento do círculo
    ctx.fill(); // Pinta o círculo
    ctx.closePath(); // Finaliza o desenho
}

// Atualiza as animações
function update() {
    color = '#' + Math.floor(Math.random() * 16777215).toString(16); // Troca a cor por uma aleatória
    if (x + dx > canvas.width || x + dx < 0) { // Se o círculo sair da tela, inverte a direção
        dx = -dx; // Inverte a direção na horizontal
    }
    if (y + dy > canvas.height || y + dy < 0) { // Se o círculo sair da tela, inverte a direção
        dy = -dy; // Inverte a direção na vertical
    }
    x += dx; // Atualiza a posição na horizontal
    y += dy; // Atualiza a posição na vertical
}


/* Faz o loop do jogo com a função draw() e update(), e usa o 
requestAnimationFrame para atualizar a tela a cada quadro */
function loop() {
    draw(); // Desenha as animações
    update(); // Atualiza as animações
    requestAnimationFrame(loop); // Atualiza a tela a cada quadro
}
loop(); // Inicia o loop da animação
