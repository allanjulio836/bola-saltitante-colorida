# ![image-1.png](./image-1.png) Bola Saltitante Colorida ![image-1.png](./image-1.png)

Olá futuro(a) programador(a)! Que tal um projeto simples (ou quase) para começar a jornada na programação?
Você está no lugar certo! Aqui neste repositório você encontrará os arquivos necessários para rodar esta simulação de animações em Javascript.

O arquivo **index.html** será o arquivo principal, você precisará dele para conseguir abrir a página do projeto em um navegador.

Já o **main.js** possui a lógica, ou seja, o código Javascript que dará funcionalidades à página, neste caso, ele será o responsável por fazer a animação da "bola saltitante" e colorida!

Sinta-se a vontade para reutilizar este código se tiver vontade. Até mais!
